#include <cstddef>
#include <iostream>

#include <unistd.h>

auto main() noexcept -> int {
  constexpr auto LEN = std::size_t{256};
  char hostname[LEN];
  const auto err = gethostname(hostname, LEN);
  if (err != 0) {
    std::cerr << "gethostname failed\n";
    return EXIT_FAILURE;
  }

  // std::cout << "hostname: " << hostname << '\n';
  std::cout << "Hostname: " << hostname << '\n';
}
