#include <cmath>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <string>

auto main(const int argc, const char *const *const argv) -> int {
  if (argc != 3) {
    std::cerr << "usage: " << argv[0] << " <float0> <float1>\n";
    // EXIT_FAILURE
    return -1;
  }

  // atof is an unsafe function
  // really should use
  // strtof and check error
  // float a = atof(argv[1]);
  // float b = atof(argv[2]);

  // float a, b;
  // if (sscanf(argv[1], "%f", &a) != 1) {
  //   return 1;
  // }
  // if (sscanf(argv[2], "%f", &b) != 1) {
  //   return 1;
  // }

  try {
    // exceptions can come from these std::stof calls
    const auto a = std::stof(argv[1]);
    const auto b = std::stof(argv[2]);

    return std::abs(a - b) > 0.1;
  } catch (const std::exception &) {
    std::cerr << "inputs failed to convert to floats\n";
    // EXIT_FAILURE
    return -1;
  }
}
