#!/bin/sh

while true; do
    timestamp=$(date +"%Y-%m-%d %H:%M:%S")
    output=$(squeue)

    echo "$timestamp - $output" >>output_log.txt

    sleep 60
done
