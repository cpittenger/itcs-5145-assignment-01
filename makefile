CFLAGS=-O3 -std=c11 -Wall -Wextra -Wpedantic -fPIC
# CFLAGS=-O3 -std=c11 -Weverything -Wno-declaration-after-statement -fPIC
CXXFLAGS=-O3 -std=c++11 -Wall -Wextra -Wpedantic -fPIC
# CXXFLAGS=-O3 -std=c++11 -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic -fPIC
LDFLAGS=
LDLIBS=libfunctions.a
LD=g++

main: main.o
	$(LD) $(LDFLAGS) main.o $(LDLIBS) -o main

prelim: prelim.o
	$(LD) $(LDFLAGS) prelim.o -o prelim

libfunctions.a: functions.o
	ar rcs libfunctions.a functions.o

plot:
	./plot.sh

test: main approx 
	./test.sh

bench: main 
	./bench.sh	

clean:
	-rm prelim prelim.o
	-rm functions.o libfunctions.a
	-rm main.o main
	-rm approx

assignment-numericalintegration.pdf: assignment-numericalintegration.tex
	pdflatex assignment-numericalintegration.tex

assignment-numericalintegration.tgz: assignment-numericalintegration.pdf \
	prelim.cpp queue_prelim.sh run_prelim.sh \
	main.cpp libfunctions.a Makefile \
	approx.cpp test.sh test_cases.txt \
	queue_job.pbs bench.sh run_bench.sh plot.sh 
	tar zcvf assignment-numericalintegration.tgz \
	assignment-numericalintegration.pdf \
	prelim.cpp queue_prelim.sh run_prelim.sh \
	main.cpp libfunctions.a Makefile \
	approx.cpp test.sh test_cases.txt \
	queue_job.pbs bench.sh run_bench.sh plot.sh 
