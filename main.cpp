#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <limits>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif

float f1(float x, int intensity);
float f2(float x, int intensity);
float f3(float x, int intensity);
float f4(float x, int intensity);

#ifdef __cplusplus
}
#endif

using fn_type = float (*)(float x, int intensity);

auto calc_numerical_integration(int fn_id, int a, int b, int n,
                                int intensity) noexcept -> double;

auto fn_id_to_fn(int fn_id) noexcept -> fn_type;

auto main(const int argc, const char *const *const argv) -> int {
  if (argc < 6) {
    std::cerr << "usage: " << argv[0] << " <fn_id> <a> <b> <n> <intensity>\n";
    return EXIT_FAILURE;
  }

  try {
    // exceptions can come from these std::stoi calls
    const auto fn_id = std::stoi(argv[1]);
    const auto a = std::stoi(argv[2]);
    const auto b = std::stoi(argv[3]);
    const auto n = std::stoi(argv[4]);
    const auto intensity = std::stoi(argv[5]);

    // t represents clock ticks which is of type 'clock_t'
    // start clock
    const auto start = std::clock();
    const auto r = calc_numerical_integration(fn_id, a, b, n, intensity);
    // end clock=
    const auto total = std::clock() - start;
    // in seconds; CLOCKS_PER_SEC is the number
    // of clock ticks per second
    const auto time_taken = static_cast<double>(total) / CLOCKS_PER_SEC;

    std::cout << r << '\n';
    std::cerr << time_taken << '\n';
  } catch (const std::exception &) {
    std::cerr << "inputs failed to convert to ints\n";
    return EXIT_FAILURE;
  }
}

auto calc_numerical_integration(const int fn_id, const int a, const int b,
                                const int n, const int intensity) noexcept
    -> double {
  const auto fn = fn_id_to_fn(fn_id);

  auto ret = 0.0;
  for (auto i = 0; i < n; ++i) {
    ret += static_cast<double>(
        fn(static_cast<float>(a) +
               (((static_cast<float>(i) + 0.5f) * static_cast<float>(b - a)) /
                static_cast<float>(n)),
           intensity));
  }

  return (static_cast<double>(b - a) * ret) / static_cast<double>(n);
}

auto fn_id_to_fn(const int fn_id) noexcept -> fn_type {
  switch (fn_id) {
  case 1:
    return f1;
  case 2:
    return f2;
  case 3:
    return f3;
  case 4:
    return f4;
  default:
    // otherwise return a function that always returns the largest float
    return [](const float, const int) {
      return std::numeric_limits<float>::max();
    };
  }
}
